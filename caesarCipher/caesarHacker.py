# Caesar Cipher Hacker
message = 'GUVF VF ZL FRPERG ZRFFNTR.'
LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

# loop through every possible key
for key in range(len(LETTERS)):
    # initialize the result string
    translated = ""

    # loop through every symbol in message
    for symbol in message:
        if symbol in LETTERS:
            index = LETTERS.find(symbol)
            index = index - key
            # wrap around if index < 0
            if index < 0:
                index = index + len(LETTERS)
            # build encrypted letter to result string
            translated = translated + LETTERS[index]
        else:
            translated = translated + symbol

    # print the current key along with its decrypted string
    print('Key #%s: %s' % (key, translated))


