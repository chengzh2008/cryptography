from utils import paperclip




def main():
    message = 'Common sense is not so common.'
    key = 8

    ciphertext = encryptMessage(key, message)
    # print the ciphertext with "|" in case there is a space at the end
    print(ciphertext + "|")
    paperclip.copy(ciphertext)

def encryptMessage(key, message):
    # each string in ciphertext represents a column in the grid
    ciphertext = [""] * key
    # loop through each column in ciphertext
    for col in range(key):
        pointer = col
        # keep looping until pointer goes past the length of the message
        while pointer < len(message):
            # put the letter at pointer in message at the end of the current column in the ciphertext list
            ciphertext[col] += message[pointer]
            # move the pointer
            pointer += key

    # convet the ciphertext list into a string
    return "".join(ciphertext)

if __name__ == "__main__":
    main()
