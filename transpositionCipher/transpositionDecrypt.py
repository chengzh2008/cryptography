# transposition cipher decryption

import math
from utils import paperclip

def main():
    message = "Cenoonommstmme oo snnio. s s c"
    key = 8

    plaintext = decryptMessage(key, message)

    print(plaintext + "|")
    paperclip.copy(plaintext)

def decryptMessage(key, message):
    # the transposition decrypt function will simulate the "columns" and "rows"
    # of the grid that the plaintext is written on by using a list of strings

    # the number of columns in the transposition grid:
    numColumns = math.ceil(1.0 * len(message) / key)

    # the number of rows in the transposition grid:
    numRows = key
    # number of shaded boxes:
    numShadedBoxes = (numColumns * numRows) - len(message)

    # each string in plaintext represents a column in the grid
    plaintext = [''] * int(numColumns)

    # initilize the point in the grid with col and row
    col = row = 0
    for symbol in message:
        plaintext[col] +=symbol
        col += 1
        # if there are no more column or we are at a shaded box, go back to the
        # first column and the next row
        if (col == numColumns) or (col == numColumns -1 and row >= numRows - numShadedBoxes):
            col = 0
            row += 1
    return "".join(plaintext)

if __name__ == "__main__":
    main()